
import {NotifyComponent} from "../core/view/NotifyComponent";
import {core} from "../core/Core";
import {NT} from "../constant/constant";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Loading extends NotifyComponent {
    // LIFE-CYCLE CALLBACKS:
    @property(cc.ProgressBar)
    public progressBar: cc.ProgressBar=null;


    @property(cc.Label)
    public progressLabel: cc.Label = null;


   @property(cc.Button)
   public startBtn: cc.Button = null ;
    

   private isComplete : boolean = false;
   public static obj : boolean = false;

    start () {
        
       this.startBtn.node.active = false;
       core.getInstance().connectTo(core.TYPE.BingoServer , 1);
       cc.loader.loadResDir("sprite"  , this.onprocess.bind(this), this.oncomplete.bind(this));
       
       
       
    //    cc.loader.loadResArray(["sprite" ,"sprite1" ,"sprite2"], (completedCount , totalCount ,item)=>{
    //     console.log(completedCount , totalCount);
    //    } , this.oncompleteLoad.bind(this));
        

        //cc.loader.loadResDir("sprite1"  , this.onprocess.bind(this) , this.oncomplete.bind(this) );
       

        
    }


    //0.3333333333333333
    onprocess(completeCount, totalCount, item){
        //console.log(cc.loader.loadResDir.length);

        
           // var persent = completeCount/totalCount;
            // this.progressLabel.string = (persent*100).toFixed(0)+"%";
            // this.progressBar.progress = persent;
           
       
                var persent = completeCount/totalCount;
                this.progressLabel.string = (persent*100).toFixed(0)+"%";
                this.progressBar.progress = persent;
            
        
            
            
    }

    

    oncomplete (err , spriteFrame){
               
        this.isComplete = true;
        this.check();


    }


    check(){

        if(this.isComplete && Loading.obj){

            this.startBtn.node.active = true;
        }

    }

    click(){

        cc.director.loadScene("Login");

    }

    
    notify(data){
        console.log(data);


            if(data.result){

                Loading.obj = true;
                this.check();
            }

    }
       

        
    
    
   
   


    // update (dt) {}

    
    
}
