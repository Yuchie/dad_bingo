/**
 *
 * @author 
 *
 */
abstract class  Observer {
	public constructor() {
    	
	}
	
    abstract notify( data: any):void;
}

// example for observer 
/* 
class ConcreteObserver  implements Observer{
    
    constructor (){
        super();
    }
    notify( data : any) :void {
    }
}
*/