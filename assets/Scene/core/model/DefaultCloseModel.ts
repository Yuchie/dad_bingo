// TypeScript file
enum CLOSTSTATE {
    TIMEOUT ,
    SERVERCLOSE ,
    NORESP ,
}
var CLOSETIME:{TIMEOUT:number , NORESP:number} = {
    TIMEOUT: 140,
    NORESP: 30
}


import  {RootData} from './RootData';
import {NETWORK} from "./../constant/core_constant";

class DefaultCloseModel extends RootData implements RootInterface{

    public type : string = "GameServer";

    public obj : string ="";

    public  respType: number =0;

    public state: number = NETWORK.STATE.CONNECTCLOSE;

    public data: any ={};

    private closeState: CLOSTSTATE;

    constructor(type: string ){
        super();
        this.type = type;
        
    }

    setRetrunData( data: any): void{

    }

    getReturnData() : void{

    };

    setData(data: any):void{

       this.closeState = data.closeState;
    }

    setSendData(data: any ) : void {

    };

    getSendData(){

    };

    getState():number{

        return this.state;
    };


}
