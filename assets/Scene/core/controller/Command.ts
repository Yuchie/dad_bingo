/**
 * Command 
 */

 import CmdReceiver from "./CmdReceiver";
 
export  class Command  {
    

    protected receiver: CmdReceiver;
    protected state:number;

    protected static command: Command;

    constructor(receiver: CmdReceiver) {
        this.receiver = receiver;
    }

    clear(){
        if(this.receiver){
            this.receiver = null;
        }
    }
    public static getInstance( receiver: CmdReceiver ):Command {

        if(this.command ===null || this.command===undefined ){
            this.command = new this(receiver);
        }
        return this.command;
    }

    execute (data: any){
        throw new Error("Abstract method!");    
    }
    getState():number{
        return this.state;
    }
}