// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";
import userLogin from "./userLogin";


const {ccclass, property} = cc._decorator;

@ccclass
export default class game extends NotifyComponent {
   

   @property(cc.Label)
   public time_label: cc.Label;

   @property(cc.Label)
   public total_time_label: cc.Label;

   @property(cc.Label)
   public state_label: cc.Label;

   @property(cc.Label)
   public period_num_label: cc.Label;

    @property(cc.Component)
    public disableBtn: cc.Component [] = [];

    @property(cc.Sprite)
    public waitingBg: cc.Sprite = null;
   
    @property(cc.Component)
    public ball_num1: cc.Component = null;

    
    @property(cc.Button)
    public masterBtn: cc.Button ;

    @property(cc.Button)
    public cancel_masterBtn: cc.Button ;

    @property(cc.Component)
    public betBtn: cc.Component = null;

    @property(cc.Component)
    public masterShow: cc.Component = null;

    @property(sp.Skeleton)
    public dragonAni: sp.Skeleton = null;

    @property(cc.Component)
    public count: cc.Component = null;
    
    @property(cc.Label)
    public gameWave: cc.Label;

    @property(cc.Label)
    public roomNum: cc.Label;

    @property(cc.Label)
    public moneyLabel: cc.Label;

    @property(sp.Skeleton)
    flipAni: sp.Skeleton[] = [];

    @property(sp.Skeleton)
    shineAni: sp.Skeleton[] = [];

    @property(cc.Component)
    result: cc.Component = null;

    @property(cc.Label)
    PlayerTitleName: cc.Label;

    @property(cc.Label)
    total_zoneId: cc.Label = null;

    @property(cc.Label)
    total_roomId: cc.Label = null;

    @property(cc.Label)
    total_win: cc.Label = null;

    @property(cc.Label)
    zoneLabel : cc.Label = null;

    @property(cc.Component)
    noticePos:　cc.Component = null;

    @property(cc.Label)
    noticePosLabel: cc.Label;

    @property(cc.Label)
    masterResultLabel:cc.Label;

    @property(cc.Component)
    menuList: cc.Component = null;

    @property(cc.WebView)
    web: cc.WebView ;

    @property(cc.Button)
    webBtn: cc.Button = null;

    @property(cc.Component)
    runBilnd: cc.Component = null;

    @property(cc.Component)
    nickWin: cc.Component = null;

    @property(cc.Component)
    nickStr: cc.Component = null;

    @property(cc.Component)
    nickWrong: cc.Component = null;

    public static master_name :string;
    public static bet_name;
    public static bet_name_pos: string;
    public static period_num : string;
    public static ball_num_s: string [] = [];
    
    public static game_time;
    private Room_master_count = 0;
    private countMoney = 0;
    public static mymoney = 0;
    public static wave;
    public static single_master_name : string;
    public static pos;
    public static checkResultPic: boolean = false;
    public static total_zone;
    public static total_room;
    public static total_money;
    public static masterResultWin: boolean = false;
    public static StateType : string;
    public static firstlogin : boolean = false;
    public static gameCanStart: boolean = false;
    // LIFE-CYCLE CALLBACKS:
    
     


    start () {
     // this.checkRoomMaster();
     
        NT.sence = 3;

        for(var a = 0; a < this.flipAni.length; a++){
            this.flipAni[a].node.active = false;
            this.shineAni[a].node.active = false;

        }
        this.nickWin.node.active = false;

        this.masterResultLabel.node.active = false;

        this.noticePos.node.active = false;

        this.result.node.active = false;

        this.roomNum.string = NT.roomNum+"廳";

        this.zoneLabel.string = "單注: "+NT.gameMoneyChoose+" 房";

        this.dragonAni.setAnimation(0, "stanby", true);
                
        this.CSGetBalance();

        this.waitingBg.node.active = false;
        
        this.checkRoomMaster();

        this.menuList.node.active = false;

        this.web.node.active = false;
        this.webBtn.node.active = false;

        this.httpsend(this.callBack, this.runBilnd);

        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'GameStatus', 
            objKey: 32,
            data:{
                
            }});

    }

    openNick(){
        this.nickWin.node.active = true;
        this.nickWrong.getComponent(cc.Label).string = "修改暱稱";
    }

    NickWrongNotice(){
        this.nickWrong.getComponent(cc.Label).string = "帳號重複";

    }
    private ni;
    NickChange(){
        
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'ChangeNickname', 
            objKey: 34,
            data:{
                newNickname: this.nickStr.getComponent(cc.Label).string,
                
            }});
        this.ni = this.nickStr.getComponent(cc.Label).string;
    }
    closeNick(){
        this.nickWin.node.active = false;

    }

    NickSuccessfulChange(){
        NT.userid = this.ni;
    }
    

    stopAllGame(){
        this.gameWave.string = "無";
        this.PlayerTitleName.string = "莊: 無";
        this.removeBackGround();
        this.removeBackGroundNum();
        this.removeResultPic();
        this.period_num_label.string = "";
        this.time_label.string = "";
        this.total_time_label.string = "";
        this.state_label.string = "停機時間";
    }



    callBack =  (blind, result)=>{
        if(result){

            this.runBilnd.getComponent("scrollchange").startScroll(result.result[0].Msg);
        }else{

        }
        
       
   }

    httpsend = (result, node)=>{
       var TimeNow= new Date();
       var yyyy = TimeNow.toLocaleDateString().slice(0,4)
       var MM = (TimeNow.getMonth()+1<10 ? '0' : '')+(TimeNow.getMonth()+1);
       var dd = (TimeNow.getDate()<10 ? '0' : '')+TimeNow.getDate();
       
       var str : string;
       var xhr = new XMLHttpRequest();
       
       xhr.onreadystatechange = ()=> {

           if (xhr.readyState == 4){
               if(xhr.status >= 200 && xhr.status < 400){
                   var response = xhr.responseText;
                   
                   if(response){
                       var responseJson = JSON.parse(response);
                       this.callBack(node, responseJson);
                   }else{
                       console.log("返回数据不存在")
                       this.callBack(node, false);
                   }
               }else{
                   console.log("请求失败")
                   this.callBack(node, false);
               }
           }

       };
       xhr.open("Get", "http://34.80.147.66:9205/API/V1/GetMarquee?date="+yyyy+"-"+MM+"-"+dd, true);
       xhr.send();

   }

    //打開玩家紀錄
    openiframe(){
        // var win = window.open();
        // win.document.write('<iframe width="560" height="315" src="//www.youtube.com/embed/mTWfqi3-3qU" frameborder="0" allowfullscreen></iframe>')
        this.web.node.active = true;
        this.web.url = "http://34.80.147.66:9205/API/V1/GutUserBet?userid="+NT.userLoginid;
        this.webBtn.node.active = true;

    }
    //關閉玩家紀錄
    closeiframe(){
        this.web.node.active = false;
        this.webBtn.node.active = false;

    }


    //最後結果
    updateFinalResult(){
        this.total_zoneId.string = "選擇金額: "+ game.total_zone ;
        this.total_roomId.string = "下注房間: "+ game.total_room+" 廳";
        this.total_win.string = "總共贏: "+game.total_money;

        
        
        if(game.single_master_name){
            this.masterResultLabel.node.active = true;
            if( game.masterResultWin){
                this.masterResultLabel.string = "莊贏";
            }else{
                this.masterResultLabel.string = "莊輸";
            }

            if(this.masterResultLabel.node.active){
                var obj = setTimeout(() => {
                   this.masterResultLabel.node.active = false; 
                }, 5000);
            }
        }

    }

    //更新波數
    updateWave(){

        
                
        if(game.single_master_name){

            this.PlayerTitleName.string = "莊: "+game.single_master_name;
            if(game.wave == 0){

                this.gameWave.string = "頭場";
                
                
            }else if(game.wave == 1){
                
                this.gameWave.string = "中場";
                
                
            }else if(game.wave == 2){
                
                this.gameWave.string = "尾場";
                
                
            }
        }else{
            this.PlayerTitleName.string = "莊: 無";
            this.gameWave.string = "無";

        }

                
    
            
            

            
    


    }

    

    //更新玩家金額
    CSGetBalance(){
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'GetBalance', 
             objKey: 30,
             data:{
                 
             }});


    }

    //取得金額後更新前端
    getBalance(){
        this.countMoney = game.mymoney;
        this.moneyLabel.string = game.mymoney+"";

    }

    //取得下注名單
    bet_list(){
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'GetBetList', 
             objKey: 18,
             data:{
                 
             }});

    }

    //下注玩家名稱
    bet_name(){
        
        if(game.StateType != "2"){
            this.clean_bet();

            

            if(game.bet_name.length > 0){

                var filtered = game.bet_name.filter(function(value) {
                    return value === NT.userid;
                });
                

                for(var a = 0; a < game.bet_name.length; a++){
                    
                    if(NT.userid != this.betBtn.getComponentsInChildren(cc.Label)[game.bet_name_pos[a]].string && this.betBtn.getComponentsInChildren(cc.Label)[game.bet_name_pos[a]].string != ""){
                       
                        
                    }else{
                        
                        if(game.bet_name[a].indexOf(game.single_master_name)==0 && game.single_master_name.indexOf(game.bet_name[a])==0){

                            this.countMoney = game.mymoney - (2*NT.gameMoneyChoose);
                            this.moneyLabel.string = this.countMoney+"";

                                this.betBtn.getComponentsInChildren(cc.Label)[game.bet_name_pos[a]].string = "莊";
                                this.betBtn.getComponentsInChildren(cc.Label)[game.bet_name_pos[a]].node.color = cc.color(255,0,0);
                            
                        }else{
                    
                            this.countMoney = game.mymoney -  (filtered.length *NT.gameMoneyChoose);
                            this.moneyLabel.string = this.countMoney+"";
                            
                            this.betBtn.getComponentsInChildren(cc.Label)[game.bet_name_pos[a]].string = game.bet_name[a];
                            this.betBtn.getComponentsInChildren(cc.Label)[game.bet_name_pos[a]].node.color = cc.color(0,0,0);
                
                        }

                    }
                }
               


                
            }
        }
    }

    open_noticePos_win(){
        
        console.log("============",game.pos, game.bet_name_pos)
        if(game.StateType == "3" && game.bet_name_pos.indexOf(game.pos) ){

            
            this.noticePosLabel.string = "目前是莊家選位";
            this.noticePos.node.active = true;


        }else if(game.StateType == "5" && game.bet_name_pos.indexOf(game.pos) && NT.userid == game.single_master_name){
            this.noticePosLabel.string = "目前是閒家選位";
            this.noticePos.node.active = true;

        }

        if(this.noticePos.node.active){

            var t1 = setTimeout(() => {
                this.noticePos.node.active = false;

            }, 1000); 

        }
        
                    
    }

    close_noticePos_win(){
        this.noticePos.node.active = false;


    }


    private betUserNum = 0;
    //計算某ID玩家壓住數量
     reg_find(val){
        var find_str = NT.userid ;
        var reg = new RegExp(find_str,"g");
        var result = val.match(reg);
        var count = (result)?result.length:0;
        this.betUserNum = count;
    }

    //關閉結算畫面
    closeResult(){
        this.result.node.active = false;
    }

    //開啟結算畫面
    openResult(){
        this.result.node.active = true;

    }

    //玩家排莊
    line_up(){
         this.sendToServer(core.TYPE.BingoServer ,{ 
           key :'LineUpMaster', 
            objKey: 24,
            data:{
                
            }});

    }

    //清理下注
    clean_bet(){

        for(var a = 0; a < this.betBtn.getComponentsInChildren(cc.Label).length; a++){
            this.betBtn.getComponentsInChildren(cc.Label)[a].string = "";

        }


    }
    
    

    //排莊顯示
    master_line_up(){
        this.bet_list();
            for(var a = 0 ; a < this.masterShow.getComponentsInChildren(cc.Label).length; a++){
                
                 this.masterShow.getComponentsInChildren(cc.Label)[a].string = game.master_name[a];
                 
                    if( this.masterShow.getComponentsInChildren(cc.Label)[a].string == "undefined"){
                        this.masterShow.getComponentsInChildren(cc.Label)[a].string = " ";

                    }

            }

    }

    private countAni = 0;
    

    //放字翻轉特效動畫
    playResultAnimation(){
        
       
        
        if(this.countAni < this.flipAni.length){
            

            this.flipAni[this.countAni].clearTrack(1);
            this.shineAni[this.countAni].clearTrack(2);
            this.flipAni[this.countAni].node.active = true;
            this.shineAni[this.countAni].node.active = true;

            

            this.flipAni[this.countAni].setAnimation(1,"draw_2", false);
            this.flipAni[this.countAni].setCompleteListener(()=>{
                
                
                this.flipAni[this.countAni].clearTrack(1);
                this.flipAni[this.countAni].node.active = false;

                


                this.shineAni[this.countAni].setAnimation(2, "draw_1", false);
                this.shineAni[this.countAni].setCompleteListener(()=>{
                    this.shineAni[this.countAni].clearTrack(2);
                    this.shineAni[this.countAni].node.active = false;

                    
                    
                    this.countAni++;
                    this.playResultAnimation2();

                
                });

          
            });

        }else{
            this.calculate();
            this.showResult();
           

        }

            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].string = game.ball_num_s[this.countAni];
            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].node.color = cc.color(0,0,0);
            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].node.scale = 1.5;   
            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].node.active= true;
            if(this.countAni < 20 ){


                if(parseInt(game.ball_num_s[this.countAni]) <= 20){
                    var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_purple");

                }else if(parseInt(game.ball_num_s[this.countAni]) > 20 && parseInt(game.ball_num_s[this.countAni]) <= 40){

                    var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_blue");

                }else if(parseInt(game.ball_num_s[this.countAni]) > 40 && parseInt(game.ball_num_s[this.countAni]) <= 60){

                    var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_yellow");

                }else if(parseInt(game.ball_num_s[this.countAni]) > 60 && parseInt(game.ball_num_s[this.countAni]) <= 80){

                    var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_black");

                }else if(parseInt(game.ball_num_s[this.countAni]) > 60 && parseInt(game.ball_num_s[this.countAni]) <= 80){

                    var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_red");
                }
                img1.x = this.ball_num1.node.children[this.countAni].x-15;
                img1.y = this.ball_num1.node.children[this.countAni].y-300;
                //img1.scale = 0.35;
                img1.parent = this.node;

                
                var img = this.getNewSprite("ballResult"+this.countAni, "sprite/resultnum/"+game.ball_num_s[this.countAni]);
                img.x = this.ball_num1.node.children[this.countAni].x-15;
                img.y = this.ball_num1.node.children[this.countAni].y-300;
                //img.scale = 0.34;
                img.parent = this.node;
            

            }


    }


    playResultAnimation2(){
    
       if(this.countAni < this.flipAni.length){
        
        

        this.flipAni[this.countAni].clearTrack(1);
        this.shineAni[this.countAni].clearTrack(2);
        this.flipAni[this.countAni].node.active = true;
        this.shineAni[this.countAni].node.active = true;
            
            this.flipAni[this.countAni].addAnimation(1,"draw_2", false);
            this.flipAni[this.countAni].setCompleteListener(()=>{
                this.flipAni[this.countAni].clearTrack(1);
                this.flipAni[this.countAni].node.active = false;


                this.shineAni[this.countAni].clearTrack(2);
                this.shineAni[this.countAni].addAnimation(2, "draw_1", false);
                this.shineAni[this.countAni].setCompleteListener(()=>{

                    this.shineAni[this.countAni].clearTrack(2);
                    this.shineAni[this.countAni].node.active = false;

                   

                    this.countAni++;
                    this.playResultAnimation();
                
                    

                
                });

            });

        }else{
            this.calculate();
            this.showResult();
            // for(var a = 0; a< this.flipAni.length; a++){
            //     this.flipAni[a].node.active = false;
            //     this.shineAni[a].node.active = false;
            // }
            
        }

        if(this.countAni < 20){
            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].string = game.ball_num_s[this.countAni];
            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].node.color = cc.color(0,0,0);
            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].node.scale = 1.5;   
            // this.ball_num1.getComponentsInChildren(cc.Label)[this.countAni].node.active= true;
            
            if(parseInt(game.ball_num_s[this.countAni]) <= 20){
                var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_purple");

            }else if(parseInt(game.ball_num_s[this.countAni]) > 20 && parseInt(game.ball_num_s[this.countAni]) <= 40){

                var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_blue");

            }else if(parseInt(game.ball_num_s[this.countAni]) > 40 && parseInt(game.ball_num_s[this.countAni]) <= 60){

                var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_yellow");

            }else if(parseInt(game.ball_num_s[this.countAni]) > 60 && parseInt(game.ball_num_s[this.countAni]) <= 80){

                var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_black");

            }else if(parseInt(game.ball_num_s[this.countAni]) > 60 && parseInt(game.ball_num_s[this.countAni]) <= 80){

                var img1 = this.getNewSprite("ballResultBack"+this.countAni, "sprite/pic/result_red");
            }
            img1.x = this.ball_num1.node.children[this.countAni].x-15;
            img1.y = this.ball_num1.node.children[this.countAni].y-300;
            //img1.scale = 0.35;
            img1.parent = this.node;
            

            var img = this.getNewSprite("ballResult"+this.countAni, "sprite/resultnum/"+game.ball_num_s[this.countAni]);
            img.x = this.ball_num1.node.children[this.countAni].x-15;
            img.y = this.ball_num1.node.children[this.countAni].y-300;
            //img.scale = 0.34;
            img.parent = this.node;
        
        
        }

    }


    //打開選單
    private menuisOpen:boolean = true;
    openMenu(){
        if(this.menuisOpen){
            this.menuList.node.active = true;
            this.menuisOpen = false;
        }else{
            this.menuList.node.active = false;
            this.menuisOpen = true;
        }


    }

    //取消排莊
    cancel_line_up(){
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'removeMaster', 
            objKey: 26,
            data:{
                
            }});

    }

    //顯示數字結果
    position_ball(){

        if(game.StateType != "2"){
        console.log("this.ball", this.ball_num1);
        if(game.ball_num_s.length > 0){
            for(var a = 0; a < game.ball_num_s.length; a++){
                
                if(parseInt(game.ball_num_s[a]) <= 20 ){

                    var img1 = this.getNewSprite("ballResultBack"+a, "sprite/pic/result_purple");

                }else if(parseInt(game.ball_num_s[a]) > 20 && parseInt(game.ball_num_s[a]) <= 40){

                    var img1 = this.getNewSprite("ballResultBack"+a, "sprite/pic/result_blue");

                }else if(parseInt(game.ball_num_s[a]) > 40 && parseInt(game.ball_num_s[a]) <= 60){

                    var img1 = this.getNewSprite("ballResultBack"+a, "sprite/pic/result_yellow");

                }else if(parseInt(game.ball_num_s[a]) > 60 && parseInt(game.ball_num_s[a]) <= 80){

                    var img1 = this.getNewSprite("ballResultBack"+a, "sprite/pic/result_black");

                }else if(parseInt(game.ball_num_s[a]) > 60 && parseInt(game.ball_num_s[a]) <= 80){

                    var img1 = this.getNewSprite("ballResultBack"+a, "sprite/pic/result_red");
                }
                img1.x = this.ball_num1.node.children[a].x-15;
                img1.y = this.ball_num1.node.children[a].y-300;
                // img1.scale = 0.35;
                img1.parent = this.node;


                var img = this.getNewSprite("ballResult"+a, "sprite/resultnum/"+game.ball_num_s[a]);
                img.x = this.ball_num1.node.children[a].x-15;
                img.y = this.ball_num1.node.children[a].y-300;
                //img.scale = 0.34;
                img.parent = this.node;
                // if(this.ball_num1.getComponentsInChildren(cc.Label)[a] != undefined){
                //     if(this.ball_num1.getComponentsInChildren(cc.Label)[a].string == "" ){
                //         this.ball_num1.getComponentsInChildren(cc.Label)[a].string = game.ball_num_s[a];
                //         this.ball_num1.getComponentsInChildren(cc.Label)[a].node.color = cc.color(0,0,0);
                //         this.ball_num1.getComponentsInChildren(cc.Label)[a].node.scale = 1.5;
                        
                //     }
                // }
            }
            
            
        }
        this.calculate();
        this.showResult();
    }
       

    }

    


   
    //放數字結果圖片
    showResult(){

        // this.removeResultPic();
        
        for(var a =0 ; a < this.count.getComponentsInChildren(cc.Sprite).length; a++){
            
            if(parseInt(this.calculateAns[a]) == 0){

                if(this.node.getComponentsInChildren(cc.Sprite)[a].node.name == "result"+a){

                }else{
                    var ran = Math.floor(NT.Math(game.period_num) * 2);

                    if(ran == 0){

                        var img = this.getNewSprite("result"+a, "sprite/num/dragon");
                        img.x = this.count.getComponentsInChildren(cc.Sprite)[a].node.x-38;
                        img.y = this.count.getComponentsInChildren(cc.Sprite)[a].node.y-330;
                        img.color = cc.color(200, 100, 100);
                        img.scale = 0.34;
                        img.parent = this.node;

                    }else if(ran == 1){

                        var img = this.getNewSprite("result"+a, "sprite/num/tiger");
                        img.x = this.count.getComponentsInChildren(cc.Sprite)[a].node.x-38;
                        img.y = this.count.getComponentsInChildren(cc.Sprite)[a].node.y-330;
                        img.color = cc.color(255, 255, 255);
                        img.scale = 0.08;
                        img.parent = this.node;

                    }
                }

            }else{
                
                var img = this.getNewSprite("result"+a, "sprite/num/"+this.calculateAns[a]);
                if(this.calculateAns[a] > 6 && this.calculateAns[a] < 10){
                    img.color = cc.color(127, 0, 255);
                }else{
                    img.color = cc.color(255,255,10);
                }
                img.x = this.count.getComponentsInChildren(cc.Sprite)[a].node.x-38;
                img.y = this.count.getComponentsInChildren(cc.Sprite)[a].node.y-330;
                
                img.parent = this.node;

                

            }
        
        
        
    }
        



    }
    //移除結果數字背景
    removeBackGround(){
        for(var a = 0; a <  this.node.getComponentsInChildren(cc.Sprite).length; a++){
         
            for(var b = 0; b < 20; b++){
                if(this.node.getComponentsInChildren(cc.Sprite)[a].node.name == "ballResultBack"+b){

                    this.node.getComponentsInChildren(cc.Sprite)[a].destroy();
                }
            }

        }

    }


    //移除結果數字
    removeBackGroundNum(){
        for(var a = 0; a <  this.node.getComponentsInChildren(cc.Sprite).length; a++){
         
            for(var b = 0; b < 20; b++){
                if(this.node.getComponentsInChildren(cc.Sprite)[a].node.name == "ballResult"+b){

                    this.node.getComponentsInChildren(cc.Sprite)[a].destroy();
                }
            }

        }


    }

    //清除計算數字
    removeResultPic(){

        for(var a = 0; a <  this.node.getComponentsInChildren(cc.Sprite).length; a++){
         
            for(var b = 0; b < 10; b++){
                if(this.node.getComponentsInChildren(cc.Sprite)[a].node.name == "result"+b){

                    this.node.getComponentsInChildren(cc.Sprite)[a].destroy();
                }
            }

        }
        

    }

    private calculateAns = [];
    //計算數字結果
    calculate(){
        this.calculateAns = [];
        var ball_num = new Array();
        var ball_num1 = new Array();
        var ball_num2 = new Array();
       // var num = new Array();
        for(var a = 0; a< game.ball_num_s.length; a++){
            
            if(a < 10){
                ball_num.push(game.ball_num_s[a].split(""));
            }else{
                ball_num1.push(game.ball_num_s[a].split(""));
            }
           
        }

        for(var a = 0; a < ball_num.length; a++){
            var num = ( parseInt(ball_num[a][0])+parseInt(ball_num[a][1])+parseInt(ball_num1[a][0])+parseInt(ball_num1[a][1]));

           
            if(num > 9){
    
                var str = num+"";
                ball_num2 = str.split("");
                this.calculateAns.push(parseInt(ball_num2[1]));
                
            }else{
                
                this.calculateAns.push(num);
            }


        }
        
       
        


    }

    //回合結束清除
    clean(){
        game.ball_num_s = [];
       
        this.calculateAns = [];
        this.countAni = 0;
        this.betUserNum = 0;
        

        for(var a = 0; a < game.ball_num_s.length; a++){
            this.ball_num1.getComponentsInChildren(cc.Label)[a].string = "";
           
        }
    }
    //本期號碼
    perioud_num_update(){
        this.period_num_label.string = game.period_num;

    }


    //取消下注
    removebetLabel(){

        
            if(NT.userid == this.betBtn.getComponentsInChildren(cc.Label)[game.pos].string){

                if(this.betBtn.getComponentsInChildren(cc.Label)[game.pos].string == "莊"){
                    // this.countMoney = game.mymoney + (2 * NT.gameMoneyChoose);
                    // this.moneyLabel.string = this.countMoney+"";

                }else{
                    var filtered = game.bet_name.filter(function(value) {
                        return value === NT.userid;
                    });
                    

                    this.countMoney = game.mymoney -  (filtered.length *NT.gameMoneyChoose);
                    this.moneyLabel.string = this.countMoney+"";
                    this.betBtn.getComponentsInChildren(cc.Label)[game.pos].string = "";


                }
            
                

            }
    


    }

    
    //玩家下注
    userBet(e, type){

        
       game.pos = parseInt(type);
       

       if(game.single_master_name != ""){
            if(this.betBtn.getComponentsInChildren(cc.Label)[parseInt(type)].string == ""){
               
                
                    this.sendToServer(core.TYPE.BingoServer ,{ 
                        key :'BetPos', 
                        objKey: 16,
                        data:{
                            pos: parseInt(type),
                        }});
                
            }else{

                
                this.sendToServer(core.TYPE.BingoServer ,{ 
                    key :'RemoveBet', 
                    objKey: 22,
                    data:{
                        pos: parseInt(type),
                        zoneId: parseInt(NT.betRoom),
                        roomId: parseInt(NT.roomNum),
                        
                    }});
                


            }
       }
        



    }

    
    //確認
    checkRoomMaster(){
        this.Room_master_count++;

        if(this.Room_master_count ==1){
            this.sendToServer(core.TYPE.BingoServer ,{ 
                key :'GetRoomMasterInfo', 
                objKey: 13,
                data:{
                   
                }});    
        }

    }
    disable(){
        
            for(var a = 0; a < this.disableBtn.length; a++){
                for(var b = 0 ; b< this.disableBtn[a].getComponents(cc.Button).length; b++){
                    this.disableBtn[a].getComponents(cc.Button)[b].enabled = false;

                }
            
            }
        

    }

    
    enable(){
   
            for(var a = 0; a < this.disableBtn.length; a++){
                for(var b = 0 ; b< this.disableBtn[a].getComponents(cc.Button).length; b++){
                    this.disableBtn[a].getComponents(cc.Button)[b].enabled = true;

                }
            
            }
        
    }
    
    backToSelectBetScene(){
        
        
        cc.director.loadScene("Bet");
        
        

    }

    getNewSprite(nodeName : string ,url: string ):cc.Node {

        var node = new cc.Node(nodeName);

        var sprite = node.addComponent(cc.Sprite);

        var deps = cc.loader.getDependsRecursively(url);

        var textures =[];

        for (var i = 0; i < deps.length; ++i) {
          
            var item = cc.loader.getRes(deps[i]);

            if (item instanceof cc.Texture2D) {
                
                var spFr = new cc.SpriteFrame();

                spFr.setTexture(item);

                sprite.spriteFrame = spFr;

            }
                
        }

        return node;
    }
    
   
    notify(data){
       // this.active();
        // this.masterBtn.enabled = false;
        // this.cancel_masterBtn.enabled = false;
        
        console.log("============================",data);

        
        if(this.node != undefined){

            if(game.gameCanStart){
                game.StateType = data.type;
            if(this.masterBtn != undefined){
                this.masterBtn.enabled = false;
                this.cancel_masterBtn.enabled = false;
            }
                //this.masterBtn.enabled = false;
                //this.cancel_masterBtn.enabled = false;
            
            
            //console.log("this.time_label_p", this.time_label.string, "data.time", data.time);
            if(this.total_time_label && this.time_label.string != undefined){
                let dateHou = new Date().getHours();
                let dateMin = new Date().getMinutes();
                let dateSec = new Date().getSeconds();

                this.total_time_label.string = dateHou+":"+dateMin+":"+dateSec;
            }

            if(this.time_label && this.time_label.string != undefined){
                this.time_label.string = data.time;
                game.game_time = data.time;
            }

            if(data.type == "0"){
                this.disable();
                game.firstlogin = false;
                if(this.state_label && this.state_label.string != undefined){
                this.state_label.string = "準備派彩";
                }
                if(this.waitingBg != undefined){
                    this.waitingBg.node.active = true;
                }
                
                

                if(this.waitingBg){
                    this.waitingBg.node.getComponentsInChildren(cc.Label)[0].string = data.time;
                    
                }
                
                

                if(parseInt(data.time) == 1){
                    this.waitingBg.node.active = false;
                    

                }
                this.removeResultPic();
                
            }

            else if(data.type == "1"){

            

                this.state_label.string = "等待派彩";

                
                this.removeResultPic();
                this.removeBackGround();
                this.removeBackGroundNum();
            
                
            
                this.masterBtn.enabled = true;
                this.cancel_masterBtn.enabled = true;
                this.disable();
                
                
                

            }else if(data.type == "2"){

            
                
                this.state_label.string = "派彩中";

                
                this.disable();

                if(parseInt(data.time) == 59){
                    
                
                    
                    this.Room_master_count = 0;
                    //this.removeResultPic();
                    
                    this.playResultAnimation();
                    this.CSGetBalance();
                    this.dragonAni.clearTrack(0);
                    this.dragonAni.setAnimation(0, "stanby2", false);
                    this.dragonAni.setCompleteListener(()=>{
                        this.dragonAni.addAnimation(0, "stanby", true);
                        
                        

                    });

                }
                if(parseInt(data.time) == 10){  
                    this.clean_bet();
                    this.clean();
                    game.checkResultPic = true;
                    this.checkRoomMaster();
                    this.openResult();
                }

                
                if(parseInt(data.time) == 1){
                    this.clean_bet();
                    
                    game.checkResultPic = false;
                }


            }else if(data.type == "3"){
                this.masterResultLabel.node.active = false;
                this.masterBtn.enabled = true;
                this.cancel_masterBtn.enabled = true;
                this.updateWave();
                this.perioud_num_update();
                this.closeResult();
                this.enable();
                this.state_label.string = "莊家選位";
                

            }else if(data.type == "4"){
                this.masterBtn.enabled = true;
                this.cancel_masterBtn.enabled = true;
                

                this.disable();
                this.state_label.string = "莊停選位";

            }else if(data.type == "5"){
                this.masterBtn.enabled = true;
                this.cancel_masterBtn.enabled = true;
            
                this.enable();
            
                this.state_label.string = "閒家選位";

            }else if(data.type == "6"){
                this.masterBtn.enabled = true;
                this.cancel_masterBtn.enabled = true;
            
                this.state_label.string = "停止下注";
                this.disable();

            }

        
            
        }
    }
}
    // update (dt) {}
}
