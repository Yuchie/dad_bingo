// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";
import game from "./game";

const {ccclass, property} = cc._decorator;

@ccclass
export default class removebetResult extends NotifyComponent {

   

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }


    notify(data){
        console.log("removeBet", data);
        
        this.node.getComponent("game").removebetLabel();

    }
    // update (dt) {}
}
