// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";


const {ccclass, property} = cc._decorator;

@ccclass
export default class bet extends NotifyComponent {

    @property(cc.Component)
    roomCom: cc.Component = null;

    @property(cc.Label)
    notice: cc.Label;

    @property(cc.Component)
    roomBack: cc.Component [] = [];

    @property(cc.Component)
    roomLabel: cc.Component[] = [];

    @property(cc.Component)
    menuList: cc.Component = null;
    // LIFE-CYCLE CALLBACKS:

    @property(cc.WebView)
    web: cc.WebView ;

    @property(cc.Button)
    webBtn: cc.Button = null;
    
    @property(cc.Component)
    playernum :cc.Component = null;

    @property(cc.Component)
    runBilnd: cc.Component = null;

    @property(cc.Component)
    nickWin: cc.Component = null;

    @property(cc.Component)
    nickStr: cc.Component = null;

    @property(cc.Component)
    nickWrong: cc.Component = null;
    // onLoad () {}
    
    start () {
        NT.sence = 2;
        this.nickWin.node.active = false;
        this.menuList.node.active = false;
        this.web.node.active = false;
        this.webBtn.node.active = false;

        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'GetRoomList', 
            objKey: 9,
            data:{
                
            }});

        this.httpsend(this.callBack, this.runBilnd);

    }

    openNick(){
        this.nickWin.node.active = true;
        this.nickWrong.getComponent(cc.Label).string = "修改暱稱";
    }

    NickWrongNotice(){
        this.nickWrong.getComponent(cc.Label).string = "帳號重複";

    }
    NickSuccessfulChange(){
        NT.userid = this.ni;
    }
    private ni;
    NickChange(){
        
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'ChangeNickname', 
            objKey: 34,
            data:{

                newNickname: this.nickStr.getComponent(cc.Label).string,
                
            }});

            this.ni = this.nickStr.getComponent(cc.Label).string;

    }

    closeNick(){
        this.nickWin.node.active = false;

    }
    openiframe(){
        // var win = window.open();
        // win.document.write('<iframe width="560" height="315" src="//www.youtube.com/embed/mTWfqi3-3qU" frameborder="0" allowfullscreen></iframe>')
        this.web.node.active = true;
        this.web.url = "http://34.80.147.66:9205/API/V1/GutUserBet?userid="+NT.userLoginid;
        this.webBtn.node.active = true;

    }

    closeiframe(){
        this.web.node.active = false;
        this.webBtn.node.active = false;

    }

    callBack =  (blind, result)=>{
        if(result){

            this.runBilnd.getComponent("scrollchange").startScroll(result.result[0].Msg);
        }else{


        }
       
   }

    httpsend = (result, node)=>{
       var TimeNow= new Date();
       var yyyy = TimeNow.toLocaleDateString().slice(0,4)
       var MM = (TimeNow.getMonth()+1<10 ? '0' : '')+(TimeNow.getMonth()+1);
       var dd = (TimeNow.getDate()<10 ? '0' : '')+TimeNow.getDate();
       
       var str : string;
       var xhr = new XMLHttpRequest();
       
       xhr.onreadystatechange = ()=> {

           if (xhr.readyState == 4){
               if(xhr.status >= 200 && xhr.status < 400){
                   var response = xhr.responseText;
                   
                   if(response){
                       var responseJson = JSON.parse(response);
                       this.callBack(node, responseJson);
                   }else{
                       console.log("返回数据不存在")
                       this.callBack(node, false);
                   }
               }else{
                   console.log("请求失败")
                   this.callBack(node, false);
               }
           }

       };
       xhr.open("Get", "http://34.80.147.66:9205/API/V1/GetMarquee?date="+yyyy+"-"+MM+"-"+dd, true);
       xhr.send();

   }
    

    private menuisOpen:boolean = true;
    openMenu(){
        if(this.menuisOpen){
            this.menuList.node.active = true;
            this.menuisOpen = false;
        }else{
            this.menuList.node.active = false;
            this.menuisOpen = true;
        }


    }


    backToRoom(){
        cc.director.loadScene("Room");


    }

    moveToGame(e, type){
        
        NT.roomNum = type;



        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'SetRoom', 
            objKey: 11,
            data:{
                roomId: parseInt(type),
            }});
        
    }
    
    updateRoomPlayer(roomNum, masterNum){
        if(this.playernum != undefined){
            for(var a =0; a < this.playernum.getComponentsInChildren(cc.Label).length; a++){
                if(roomNum.length > 0){
                    this.playernum.getComponentsInChildren(cc.Label)[a].string = "   人數:"+roomNum[a] + "排莊:"+masterNum[a];
                }else{
                    this.playernum.getComponentsInChildren(cc.Label)[a].string = "   人數:0"+" 排莊:0 ";
                }
                

            }


        }


    }

    onEditDidBegan(editbox, customEventData) {
        
        
        
        // 这里 editbox 是一个 cc.EditBox 对象
        // 这里的 customEventData 参数就等于你之前设置的 "foobar"
    }

      // 假设这个回调是给 editingDidEnded 事件的
      onEditDidEnded(editbox, customEventData) {
         
  
       

        // 这里 editbox 是一个 cc.EditBox 对象
        // 这里的 customEventData 参数就等于你之前设置的 "foobar"
    }


    private betMoneyRoom = [] ;
    private playernumArr = [];
    
    notify(data){

        console.log("roomBet", data);
        this.updateRoomPlayer(data.roomNum, data.masterNum);
        this.betMoneyRoom = NT.betRoom;
        
            if(NT.betRoom == 0 ){
                    
                NT.gameMoneyChoose = 30;
                this.notice.string = "選擇金額:  30";
               

                    for(var a = 0; a < this.roomBack.length; a++){
                        
                          var img = this.getNewSprite("background"+a, "sprite/pic/lobby_yellow");

                          img.x = this.roomBack[a].node.x-40;
                          img.y = this.roomBack[a].node.y-250;
                          img.scale = 0.9;
                          img.parent = this.node;
                          

                          var img1 = this.getNewSprite("label"+a, "sprite/bet/bet_0");
                          img1.x = this.roomBack[a].node.x-40;
                          img1.y = this.roomBack[a].node.y-250;
                          img1.scale = 0.9;
                          img1.parent = this.node;
                    }
                          
                     
                        

           
                

             
                
               
               
               // this.roomBack[a].getComponents(cc.Sprite)[0].spriteFrame = new cc.SpriteFrame(cc.url.raw(s));
            }else if (NT.betRoom == 1){
                NT.gameMoneyChoose = 50;
                this.notice.string = "選擇金額:  50";
                for(var a = 0; a < this.roomBack.length; a++){
                        
                    var img = this.getNewSprite("background"+a, "sprite/pic/lobby_green");

                    img.x = this.roomBack[a].node.x-40;
                    img.y = this.roomBack[a].node.y-250;
                    img.scale = 0.9;
                    img.parent = this.node;

                    var img1 = this.getNewSprite("label"+a, "sprite/bet/bet_1");
                    img1.x = this.roomBack[a].node.x-40;
                    img1.y = this.roomBack[a].node.y-250;
                    img1.scale = 0.9;
                    img1.parent = this.node;
              }
    
            }else if(NT.betRoom == 2){

                NT.gameMoneyChoose = 100;
                this.notice.string = "選擇金額:  100";
                for(var a = 0; a < this.roomBack.length; a++){
                        
                    var img = this.getNewSprite("background"+a, "sprite/pic/lobby_purple");

                    img.x = this.roomBack[a].node.x-40;
                    img.y = this.roomBack[a].node.y-250;
                    img.scale = 0.9;
                    img.parent = this.node;

                    var img1 = this.getNewSprite("label"+a, "sprite/bet/bet_2");
                    img1.x = this.roomBack[a].node.x-40;
                    img1.y = this.roomBack[a].node.y-250;
                    img1.scale = 0.9;
                    img1.parent = this.node;
              }
    
    
            }else if(NT.betRoom == 3){

                NT.gameMoneyChoose = 300;
                this.notice.string = "選擇金額:  300";

                for(var a = 0; a < this.roomBack.length; a++){
                        
                    var img = this.getNewSprite("background"+a, "sprite/pic/lobby_blue");

                    img.x = this.roomBack[a].node.x-40;
                    img.y = this.roomBack[a].node.y-250;
                    img.scale = 0.9;
                    img.parent = this.node;

                    var img1 = this.getNewSprite("label"+a, "sprite/bet/bet_3");
                    img1.x = this.roomBack[a].node.x-40;
                    img1.y = this.roomBack[a].node.y-250;
                    img1.scale = 0.9;
                    img1.parent = this.node;
              }
    
            }else if(NT.betRoom == 4){

                NT.gameMoneyChoose = 500;
                this.notice.string = "選擇金額:  500";

                 for(var a = 0; a < this.roomBack.length; a++){
                        
                          var img = this.getNewSprite("background"+a, "sprite/pic/lobby_red");

                          img.x = this.roomBack[a].node.x-40;
                          img.y = this.roomBack[a].node.y-250;
                          img.scale = 0.9;
                          img.parent = this.node;

                          var img1 = this.getNewSprite("label"+a, "sprite/bet/bet_4");
                          img1.x = this.roomBack[a].node.x-40;
                          img1.y = this.roomBack[a].node.y-250;
                          img1.scale = 0.9;
                          img1.parent = this.node;
                    }

    
            }else if(NT.betRoom == 5){

                NT.gameMoneyChoose = 1000;
                this.notice.string = "選擇金額:  1000";

                for(var a = 0; a < this.roomBack.length; a++){
                        
                          var img = this.getNewSprite("background"+a, "sprite/pic/lobby_black");

                          img.x = this.roomBack[a].node.x-40;
                          img.y = this.roomBack[a].node.y-250;
                          img.scale = 0.9;
                          img.parent = this.node;

                          var img1 = this.getNewSprite("label"+a, "sprite/bet/bet_5");
                          img1.x = this.roomBack[a].node.x-40;
                          img1.y = this.roomBack[a].node.y-250;
                          img1.scale = 0.9;
                          img1.parent = this.node;
                    }

            }
            
            this.notice.node.color = cc.color(231, 179, 14);
        

       

    }

    
    
    
    

           

    getNewSprite(nodeName : string ,url: string ):cc.Node {

        var node = new cc.Node(nodeName);

        var sprite = node.addComponent(cc.Sprite);

        var deps = cc.loader.getDependsRecursively(url);

        var textures =[];

        for (var i = 0; i < deps.length; ++i) {
          
            var item = cc.loader.getRes(deps[i]);

            if (item instanceof cc.Texture2D) {
                
                var spFr = new cc.SpriteFrame();

                spFr.setTexture(item);

                sprite.spriteFrame = spFr;

            }
                
        }

        return node;
    }
    // update (dt) {}
}
