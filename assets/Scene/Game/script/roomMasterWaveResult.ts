// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";
import game from "./game";

const {ccclass, property} = cc._decorator;

@ccclass
export default class roomMasterWaveResult extends NotifyComponent {

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

   

    notify(data){

        console.log("RoomMasterWave", data);
        game.period_num = data.nop;
        game.master_name = data.nickname;
        game.wave = data.wave;
        
        game.single_master_name = data.mnickname;
        
        

            if(game.StateType == "0" || game.StateType == "1"){
                

            }else{
                this.node.getComponent("game").master_line_up();
                this.node.getComponent("game").perioud_num_update();
                this.node.getComponent("game").updateWave();

            }
           
            // for(var a = 0; a < data.ball.length; a++){
            //     game.ball_num_s.push(data.ball[a]);
        
            // }
            game.ball_num_s = data.ball;
           

            if(game.checkResultPic == false){
                this.node.getComponent("game").position_ball();
            }
              
        
    }
    // update (dt) {}
}
