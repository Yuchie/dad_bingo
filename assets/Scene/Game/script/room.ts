// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";
import scrollchange from "./scrollchange";

const {ccclass, property} = cc._decorator;

@ccclass
export default class room extends NotifyComponent {

    
    @property(cc.Component)
    roomCom: cc.Component = null;

    @property(cc.Component)
    MasterNum: cc.Component [] = [];

    @property(cc.Component)
    userNum: cc.Component[] = [];

    @property(cc.Component)
    menuList: cc.Component = null;

    @property(cc.WebView)
    web: cc.WebView ;

    @property(cc.Button)
    webBtn: cc.Button = null;

    @property(cc.Component)
    runBilnd: cc.Component = null;

    @property(cc.Component)
    nickWin: cc.Component = null;

    @property(cc.Component)
    nickStr: cc.Component = null;

    @property(cc.Component)
    nickWrong: cc.Component = null;
    
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    
    start () {
        NT.sence = 1;

        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'GetZoneList', 
            objKey: 5,
            data:{
                
            }});
        this.menuList.node.active = false;
        this.web.node.active = false;
        this.webBtn.node.active = false;
        this.nickWin.node.active = false;
        

        this.httpsend(this.callBack, this.runBilnd);

        
        //this.runBilnd.getComponent("scrollchange").startScroll(this.scrollChange(arry));
    }
    openNick(){

        this.nickWin.node.active = true;
        this.nickWrong.getComponent(cc.Label).string = "修改暱稱";

    }
    NickWrongNotice(){
        this.nickWrong.getComponent(cc.Label).string = "帳號重複";

    }
    NickSuccessfulChange(){
        NT.userid = this.ni;
    }
    private ni;
    NickChange(){
        
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'ChangeNickname', 
            objKey: 34,
            data:{

                newNickname: this.nickStr.getComponent(cc.Label).string,
                
            }});
            this.ni = this.nickStr.getComponent(cc.Label).string;
    }

    closeNick(){
        this.nickWin.node.active = false;

    }
     
    callBack =  (blind, result)=>{
        if(result){

            this.runBilnd.getComponent("scrollchange").startScroll(result.result[0].Msg);
        }else{

        }
        
    }

     httpsend = (result, node)=>{
        var TimeNow= new Date();
        var yyyy = TimeNow.toLocaleDateString().slice(0,4)
        var MM = (TimeNow.getMonth()+1<10 ? '0' : '')+(TimeNow.getMonth()+1);
        var dd = (TimeNow.getDate()<10 ? '0' : '')+TimeNow.getDate();
        
        var str : string;
        var xhr = new XMLHttpRequest();
        
        xhr.onreadystatechange = ()=> {

            if (xhr.readyState == 4){
                if(xhr.status >= 200 && xhr.status < 400){
                    var response = xhr.responseText;
                    
                    if(response){
                        var responseJson = JSON.parse(response);
                        this.callBack(node, responseJson);
                    }else{
                        console.log("返回数据不存在")
                        this.callBack(node, false);
                    }
                }else{
                    console.log("请求失败")
                    this.callBack(node, false);
                }
            }

        };
        xhr.open("Get", "http://34.80.147.66:9205/API/V1/GetMarquee?date="+yyyy+"-"+MM+"-"+dd, true);
        xhr.send();

    }
    
    
    openiframe(){
        // var win = window.open();
        // win.document.write('<iframe width="560" height="315" src="//www.youtube.com/embed/mTWfqi3-3qU" frameborder="0" allowfullscreen></iframe>')
        this.web.node.active = true;
        this.web.url = "http://34.80.147.66:9205/API/V1/GutUserBet?userid="+NT.userLoginid;
        this.webBtn.node.active = true;
        // var url = "http://34.80.147.66:9205/API/V1/GutUserBet?userid="+NT.userLoginid;
        // console.log(url);
        // var xmlHttp = new XMLHttpRequest();
        // xmlHttp.open("get", url);
        

    }
    
    
    scrollchange(result){

        console.log("this.node===================", result);

    }

       
        
    

    

    closeiframe(){
        this.web.node.active = false;
        this.webBtn.node.active = false;

    }

    private menuisOpen:boolean = true;
    openMenu(){
        if(this.menuisOpen){
            this.menuList.node.active = true;
            this.menuisOpen = false;
        }else{
            this.menuList.node.active = false;
            this.menuisOpen = true;
        }


    }

    backToLogin(){
        cc.director.loadScene("Login");

    }

    click(e, type){
        
        NT.betRoom = parseInt(type);
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'SetZone', 
            objKey: 7,
            data:{
                zoneId: parseInt(type),
            }});
         
        

    }


    notify(data){

        console.log(data);

        for(var  a= 0; a < data.zone.length; a++){
         //   this.roomCom.getComponentsInChildren(cc.Label)[a].string = data.zone[a].betMoney;
            this.MasterNum[a].getComponent(cc.Label).string = "排莊: "+data.zone[a].masterNum;
            this.userNum[a].getComponent(cc.Label).string = "人數: " +data.zone[a].userNum;
           // this.userNum[a].getComponent(cc.Label).string = "人數: "+data.zone[a].userNum;
        }
        


    }
    // update (dt) {}
}
