// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";
import game from "./game";

const {ccclass, property} = cc._decorator;

@ccclass
export default class getWinResult extends NotifyComponent {

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    calculate(arr){
        var sum=0;

        for (var i = 0; i < arr.length; i++) {
            sum += arr[i];
        };

        return sum;

    }

    calculateMasterWin(roomId, win){
        var sum = 0;
        for(var a = 0; a < roomId.length; a++){
            if(roomId[a] == NT.roomNum){

                sum += win[a];


            }



        }
        if(sum > 0){
            game.masterResultWin = true;
        }else{
            game.masterResultWin = false;

        }



    }
    // update (dt) {}

    notify(data){

        console.log("=====================BALLRESULT", data.ballResult, data.nop, data);

        var roomResult = data.roomId.filter(function(element, index, arr){
            return arr.indexOf(element) === index;
        });

        var zoneResult = data.zoneId.filter(function(element, index, arr){
            return arr.indexOf(element) === index;
        });

        this.calculateMasterWin(data.roomId, data.win);

        game.total_room = roomResult;
        game.total_zone = zoneResult;
        game.ball_num_s = data.ballResult;
        game.period_num = data.nop;
        game.total_money = this.calculate(data.win);



        this.node.getComponent("game").updateFinalResult();
       // this.node.getComponent("game").perioud_num_update();


       

    }
}
