import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";

const {ccclass, property} = cc._decorator;

@ccclass
export default class userLogin extends NotifyComponent {

    @property(cc.Component)
    username: cc.Component = null;

    @property(cc.Component)
    password:cc.Component = null;
    
    @property(cc.Label)
    user: cc.Label ;

    @property(cc.Label)
    pass: cc.Label;

    @property(cc.Component)
    window: cc.Component = null;

    @property(sp.Skeleton)
    logo: sp.Skeleton = null;

    @property(cc.Component)
    scrollview: cc.Component = null;

    @property(cc.Component)
    agreeBtn: cc.Component = null;

    @property(cc.Component)
    rejectBtn: cc.Component = null;

    public static errorObj : boolean = false;
    
    start () {
        // init logic
        this.window.node.active = false;

        this.logo.setAnimation(1, "logo_1", false);
        this.logo.setCompleteListener(()=>{
            this.logo.addAnimation(1, "logo_2", true);
        });

        this.scrollview.node.active = false;
        this.agreeBtn.node.active = false;
        this.rejectBtn.node.active = false;
        

    }



    getUserInfo(){
        
        this.sendToServer(core.TYPE.BingoServer ,{ 
            key :'Auth', 
            objKey: 3,
            data:{
                username : this.username.getComponent(cc.Label).string,
                pwd: this.password.getComponent(cc.Label).string,
            }});
              

        
    }

    OnScrollEnd(){
        this.agreeBtn.node.active = true;
        this.rejectBtn.node.active = true;

    }

    agree(){
        cc.director.loadScene("Room");

    }

    reject(){
        this.scrollview.node.active = false;
        this.agreeBtn.node.active = false;
        this.rejectBtn.node.active = false;

    }

    windowClose(){
        this.window.node.active = false;

    }
    
    incorrectInfo(){

        
            this.window.node.active = true;


    }

    onEditDidBegan(editbox, customEventData) {
        if(customEventData == "1"){
            this.user.node.active = false;

        }else{
            this.pass.node.active = false;
        }
        
        
        // 这里 editbox 是一个 cc.EditBox 对象
        // 这里的 customEventData 参数就等于你之前设置的 "foobar"
    }

      // 假设这个回调是给 editingDidEnded 事件的
      onEditDidEnded(editbox, customEventData) {
          if(customEventData == "1"){
            this.user.node.active = true;
            this.user.string = this.username.getComponent(cc.Label).string;
          }else{
            this.pass.node.active = true;
            this.pass.string = this.password.getComponent(cc.Label).string;
          }
  
       

        // 这里 editbox 是一个 cc.EditBox 对象
        // 这里的 customEventData 参数就等于你之前设置的 "foobar"
    }

    



        notify(data){
            //console.log("fewfewfew",data);
            console.log(data);
            if(data.result){
                console.log("===========================================", data);
                NT.money = data.mymoney;
                //NT.userid = this.user.string;
                NT.userid = data.nickname;
                NT.userLoginid = this.user.string;
                this.scrollview.node.active = true;
                this.scrollview.node.on("bounce-bottom", this.OnScrollEnd, this);
                


            }else{
                this.window.node.active = true;
            }

        }

    }





