const {ccclass, property} = cc._decorator;

@ccclass
export default class scrollchange extends cc.Component {

    @property(cc.Node) maskNode: cc.Node = null;
    @property(cc.Label) label: cc.Label = null;
    
    /**
     * 滾動內容
     */
    contentArr:Array<string> = new Array<string>()

    startPos:cc.Vec2 = null

    onLoad()
    {
       // this.startPos = cc.v2(this.maskNode.width/2,0);
       this.startPos = cc.v2(this.maskNode.width);
        if(this.contentArr.length == 0)
        {
            this.node.active = false;
        }
        this.label.node.position = this.startPos;
        //cc.find("Canvas/Btn").on(cc.Node.EventType.MOUSE_UP,()=>{
            // this.startScroll("fk off");
       // })
    }

    /**
     * 開始滾動信息
     * @param content 滾動內容
     */
    startScroll(content:string):void
    {
        let self = this;
        if(content == null || content.length == 0)
        {
            return;
        }
        this.node.active = true;
        this.contentArr.push(content);
        if(self.label.node.getActionByTag(0) != null && this.label.node.getActionByTag(0).isDone() == false)//如果正在播放只插入數據
        {
            return;
        }

        let scrollFunc = function()
        {
            if(self.contentArr.length > 0)
            {
                self.label.string = self.contentArr.shift();
                //需要先更新標籤的寬度，不然下一幀才更新，這裏取到的值就會是原來的值，導致寬度計算錯誤
                self.label._forceUpdateRenderData(true);
                self.label.node.position = self.startPos;
                //let distance:number = (self.label.node.width + self.label.node.parent.width)-120;
                let distance:number = (self.label.node.width + self.label.node.parent.width)+500;
                let duration: number = distance / 100;
                let seq = cc.sequence(
                    cc.moveBy(duration,cc.v2(-distance,0)),
                    cc.callFunc(function(){
                        self.label.string = content;
                        self.label.node.position = self.startPos;
                        scrollFunc();
                    })
                )
                seq.setTag(0);
                self.label.node.runAction(seq);
            }
            else
            {
                
                self.label.string = content;
                //需要先更新標籤的寬度，不然下一幀才更新，這裏取到的值就會是原來的值，導致寬度計算錯誤
                self.label._forceUpdateRenderData(true);
                self.label.node.position = self.startPos;
                let distance:number = (self.label.node.width + self.label.node.parent.width)-120;
                let duration: number = distance / 100;
                let seq = cc.sequence(
                    cc.moveBy(duration,cc.v2(-distance,0)),
                    cc.callFunc(function(){
                        //self.label.string = "";
                        self.label.node.position = self.startPos;
                        scrollFunc();
                    })
                )
                seq.setTag(0);
                self.label.node.runAction(seq);
            }
        }
        scrollFunc();
    }
    onDestroy()
    {
        // if(this.label.node.getActionByTag(0) != null )
        // {
        //      this.label.node.stopAction(this.label.node.getActionByTag(0));
        // }
    }
}
