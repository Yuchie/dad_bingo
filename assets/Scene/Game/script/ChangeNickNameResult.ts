// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {NotifyComponent} from "../../core/view/NotifyComponent";
import {core} from "../../core/Core";
import {NT} from "../../constant/constant";


const {ccclass, property} = cc._decorator;

@ccclass
export default class ChangeNickNameResult extends NotifyComponent {

  

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }


    notify(data){
        console.log("ChangeNickNameResult", data);
        
        if(data.result){
            if(NT.sence == 1){
                this.node.getComponent("room").closeNick();
                this.node.getComponent("room").NickSuccessfulChange();
            }else if(NT.sence == 2){
                this.node.getComponent("bet").closeNick();
                this.node.getComponent("bet").NickSuccessfulChange();

            }else if(NT.sence == 3){
                this.node.getComponent("game").closeNick();
                this.node.getComponent("game").NickSuccessfulChange();

            }
        }else{
            if(NT.sence == 1){
                this.node.getComponent("room").NickWrongNotice();

            }else if(NT.sence == 2){
                this.node.getComponent("bet").NickWrongNotice();

            }else if(NT.sence == 3){
                this.node.getComponent("game").NickWrongNotice();

            }


        }

        

    }
    // update (dt) {}
}
