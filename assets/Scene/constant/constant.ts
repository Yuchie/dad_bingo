

export class NT{
    public static protoUrl : string[] = ["./proto/BingoServer"];
    public static protoName : string[] =["BingoServer"];
    //0 : json , 1 : protobuf , 2 :json 
    public static MSGTYPE  = 1;

    public static STATE = {
        Onconnect :     2,
        Auth :   3,
        AuthConnectResult :  4,
        GetZoneList: 5 ,
        ZoneList : 6 ,
        SetZone : 7 ,
        SetZoneResult : 8,
        GetRoomList: 9,
        RoomList: 10,
        SetRoom: 11,
        SetRoomResult: 12,
        GetRoomMasterInfo: 13,
        RoomMasterWave: 14,
        RoomMasterSort: 15,
        BetPos: 16,
        BetResult: 17,
        GetBetList: 18,
        BetList: 19,
        Counting: 20,
        WinResult: 21,
        RemoveBet: 22,
        RemoveBetReuslt: 23,
        LineUpMaster: 24,
        LineUpMasterResult: 25,
        removeMaster: 26,
        removeMasterResult: 27,
        LineUpData: 28,
        GetLineUpData:29,
        GetBalance:30,
        GetBalacneResult:31,
        GameStatus:32,
        GameStatusResult: 33,
        ChangeNickname: 34, 
        ChangeNicknameResult: 35,
        Error : 9999
    };

    public  static Config = [
        {
            TYPE:"LOCAL",
            HOST: '',
            PORT : -1,
            isSSL : false
        },{
            TYPE:"BingoServer",
            HOST: "34.80.147.66",
            PORT : 10710,
            isSSL : false
        }
    ];
    static userLoginid: any;
    static token: any;
    static currency: any;
    static currencyRate: any;
    static money: any;
    static gameName: any;
    static isSSL : any;
    static betRoom: any;
    static gameMoneyChoose: any;
    static roomNum: any;
    static userid: any;
    static sence: any;
    static Math = function(seed){
        return parseFloat('0.'+Math.sin(seed).toString().substr(6));
    }
    
}

// export NT;